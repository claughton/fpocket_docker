PREFIX ?= /usr/local
VERSION = "3.0"

all: install

install:
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	install -m 0755 scripts/fpocket $(DESTDIR)$(PREFIX)/bin/fpocket
	install -m 0755 scripts/mdpocket $(DESTDIR)$(PREFIX)/bin/mdpocket
	install -m 0755 scripts/dpocket $(DESTDIR)$(PREFIX)/bin/dpocket
	install -m 0755 scripts/tpocket $(DESTDIR)$(PREFIX)/bin/tpocket

uninstall:
	@$(RM) $(DESTDIR)$(PREFIX)/bin/fpocket
	@$(RM) $(DESTDIR)$(PREFIX)/bin/mdpocket
	@$(RM) $(DESTDIR)$(PREFIX)/bin/dpocket
	@$(RM) $(DESTDIR)$(PREFIX)/bin/tpocket
	@docker rmi claughton/fpocket:$(VERSION)

build: 
	@docker build -t claughton/fpocket:$(VERSION) -f Dockerfile . 
	@docker tag claughton/fpocket:$(VERSION) claughton/fpocket:latest

publish: build
	@docker push claughton/fpocket:$(VERSION) 
	@docker push claughton/fpocket:latest 

.PHONY: all install uninstall build publish 
