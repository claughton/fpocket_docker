# fpocket_docker

Delivers [fpocket](https://github.com/Discngine/fpocket) via a Docker container.

## Installation

```
% pip install git+https://bitbucket.org/claughton/fpocket_docker.git
```
This will install the four key Fpocket commands in your path:

* fpocket: the original pocket prediction on a single protein structure
* mdpocket: extension of fpocket to analyse conformational ensembles of proteins (MD trajectories for instance)
* dpocket: extract pocket descriptors
* tpocket: test your pocket scoring function

**Note:** Netcdf support in mdpocket has been disabled as a workaround to [issue 9](https://github.com/Discngine/fpocket/issues/9)

## Usage

Seee the official Fpocket documentation [here](https://github.com/Discngine/fpocket)

