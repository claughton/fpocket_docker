from setuptools import setup, find_packages
setup(
    name = 'fpocketdocker',
    version = '3.0',
    packages = find_packages(),
    scripts = [
        'scripts/fpocket',
        'scripts/mdpocket',
        'scripts/dpocket',
        'scripts/tpocket',
    ],
)
