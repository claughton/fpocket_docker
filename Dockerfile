FROM ubuntu:latest
RUN apt-get update && apt-get install libnetcdf-dev git make -y
RUN cd /tmp &&\
    git clone https://github.com/Discngine/fpocket.git
COPY mdpocket.c /tmp/fpocket/src/mdpocket.c
RUN cd /tmp/fpocket &&\
    make &&\
    make install
RUN cd /tmp &&\
    rm -rf fpocket
RUN echo '#!/bin/bash' > /usr/local/bin/run && \
    echo 'exec $@' >> /usr/local/bin/run && \
    chmod +x /usr/local/bin/run
WORKDIR /wd
ENTRYPOINT ["/usr/local/bin/fpocket"]
CMD ["-h"]
